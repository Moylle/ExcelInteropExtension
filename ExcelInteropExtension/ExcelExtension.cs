﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace ExcelInteropExtension
{
    public static class ExcelExtension
    {
		public static Range GetMiddleCell(this Shape s)
		{
			Range cel = s.TopLeftCell;
			Range ran = cel.EntireColumn;
			float h = 0;
			foreach(Range r in ran.Cells)
			{
				h += r.Height;
				if(r.Row == cel.Row)
					break;
			}
			ran = cel.EntireRow;
			float w = 0;
			foreach(Range r in ran.Cells)
			{
				w += r.Width;
				if(r.Column == cel.Column)
					break;
			}
			if(h - s.Top < s.Height / 2)
				cel = cel.Offset[1, 0];
			if(w - s.Left < s.Width / 2)
				cel = cel.Offset[0, 1];
			return cel;
		}
    }
}
